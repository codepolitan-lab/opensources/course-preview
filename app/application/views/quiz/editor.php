<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
<style>
.bottombar {padding: 10px;border-top: 1px solid #ddd;position: fixed;bottom: 0;width: 100%;background: #fafafa;}
.question {font-size: 16px;}
.question-content {font-size: 18px; margin-bottom: 20px; border-bottom: 1px solid #eee;}
.quizform {width: 43%;position: fixed;height: 100%;right: -675px;top: 0;background: #fafafa;padding: 20px;border-left: 1px solid #ddd;overflow: auto; transition: .2s all;}
.option-form textarea {margin-bottom: 10px;}
.option-area {position: relative;}
.option-area a.delete {position: absolute;top: 0;right: 0;padding: 0 5px;background: orange;color: white;border-radius: 3px 0 3px 3px;}
.option-label {padding: 10px 10px 10px 30px !important; display: block; border-radius: 5px;}
.option-label:hover {background-color: #eee;}
.option-label > p:last-child {margin:0;}
.isright {border: 2px solid lime;}
.edit-question {position: absolute; right:16px;}
.CodeMirror, .CodeMirror-scroll {min-height: 100px;}
</style>
<div class="container" style="margin-bottom: 60px">
	
	<div class="row">
		<h1>Quiz Editor</h1>
		<p>Course: <strong><?= $course; ?></strong>, Topic: <strong><?= $topic; ?></strong></p>
		<hr>

		<div class="col-md-7"  id="quizzes">
			<h2>Quiz</h2>

			<?php if(!empty($quiz['questions'])): ?>
				
				<?php foreach ($quiz['questions'] as $i => $question): ?>
				<div class="panel panel-default question" data-id="<?= $i; ?>" data-title="<?= $question['question_title']; ?>" data-content="<?= base64_encode($question['question_content']); ?>">
					<button type="button" class="btn btn-sm edit-question" id="<?= $i; ?>">edit</button>
					<div class="panel-body">
						<div class="question-content"><?= $parsedown->text($question['question_content']); ?></div>
						<ul class="list-unstyled"`>
							<?php foreach ($question['options'] as $j => $option): ?>
							<li class="radio" data-isright="<?= $option['is_right']; ?>" data-content="<?= base64_encode($option['option_content']); ?>">
								<label class="option-label <?= $option['is_right'] ? 'isright' : ''; ?>">
									<input type="radio" name="<?= $i; ?>">
									<?= $parsedown->text($option['option_content']); ?>
								</label>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<?php endforeach; ?>

			<?php else: ?>
				<p class="text-muted"><em>Save quiz first then add question</em></p>
			<?php endif; ?>
	
		</div>
		<div class="col-md-5">
			<h2>Information</h2>

			<div class="form-group">
				<label>Quiz Title</label>
				<input type="text" class="form-control" value="<?= $quizfile; ?>" name="title" id="title" disabled>
			</div>
			<div class="form-group">
				<label>Description</label>
				<textarea class="form-control" name="description" id="description" rows="5"><?= $quiz['description'] ?? ''; ?></textarea>
			</div>
			<div class="form-group">
				<label>Duration</label>
				<input type="text" class="form-control" value="<?= $quiz['duration'] ?? ''; ?>" name="duration" id="duration">
			</div>
			<div class="form-group">
				<label>Nilai Ketuntasan Minimum (%)</label>
				<input type="text" class="form-control" value="<?= $quiz['kkm'] ?? ''; ?>" name="kkm" id="kkm">
			</div>
			<label>Randomness</label>
			<div class="checkbox">
				<label>
					<input type="checkbox" <?= $quiz['random_question'] ?? null ? 'checked' : '';?> name="random_question" id="random_question"> Random Questions
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" <?= $quiz['random_answer'] ?? null ? 'checked' : '';?> name="random_answer" id="random_answer"> Random Options
				</label>
			</div>
		</div>
	</div>
</div>

<div class="bottombar">
	<div class="btn-group">
		<a href="<?= site_url('quiz/index/'.$course); ?>" class="btn btn-default">&laquo; Back</a>
	</div>
	<div class="btn-group pull-right">
		<?php if($quizfile): ?>
			<button class="btn btn-success" id="btn-show-form">+ Add new question</button>
		<?php endif; ?>
		<button class="btn btn-primary" id="save-quiz">Save Quiz</button>
	</div>
</div>

<div class="quizform">
	<h3>Question form</h3>
	<hr>
	<div class="form-group">
		<label>Question</label>
		<textarea name="question_content" class="form-control" rows="5"></textarea>
	</div>
	<hr>
	<div class="form-group">
		<label>Answer option</label>
		<div class="option-form">
			<div class="option-area">
				<div class="checkbox"><label><input type="checkbox" name="answer[]" value="y"> This is answer</label></div>
				<textarea name="options[]" class="form-control answers" rows="3"></textarea>
				<a href="#" class="delete">x</a>
			</div>
		</div>
		<hr>
		<button class="btn btn-link" id="add-option">+ Add new option</button>
		<hr>
		<button class="btn btn-primary" id="save-question">Save Question</button>
		<button class="btn btn-default" id="close-form">Cancel</button>
	</div>
</div>
<script type="text/javascript">
	var questions = [];
	var qtotal = 0;
	$(function(){
		var mdquestion = new SimpleMDE({ element: $("textarea[name=question_content]")[0] });
		
		$('.question').each(function(i){
			var q = {
				question_title: $(this).data('title'),
				question_content: $(this).data('content'),
				options: []
			};

			var radio = $(this).children('.panel-body').children('ul').children('li.radio');
			radio.each(function(ii){
				var o = {
					is_right: $(this).data('isright'),
					option_content: $(this).data('content'),
				};		
				q.options.push(o);
			});
			
			questions.push(q);
			qtotal++;
		})


		$('#add-option').on('click', function(e){
			e.preventDefault();
			$('.option-form').append('<div class="option-area"><div class="checkbox"><label><input type="checkbox" name="answer[]" value="y"> This is answer</label></div><textarea name="options[]" class="form-control" rows="3"></textarea><a href="#" class="delete">x</a></div>');
		});
		$('.delete').on('click', function(e){
			e.preventDefault();
			if(confirm('Serius mau dihapus?'))
				$(this).closest('.option-area').remove();
			return;
		})
		$('#close-form').on('click', function(){
			$('.quizform').css('right','-675px');
		})
		$('#btn-show-form').on('click', function(){
			$('.quizform').css('right','0');
			$('textarea[name=question_content]').focus();
		})
		$('#save-quiz').on('click', function(){
			var quiz = {
				title: $('#title').val(),
				description: $('#description').val(),
				duration: $('#duration').val(),
				kkm: $('#kkm').val(),
				random_question: $('#random_question').prop('checked') ? 1 : 0,
				random_answer: $('#random_answer').prop('checked') ? 1 : 0,
				questions: questions
			};
			console.log(quiz);
			$.ajax({
				method: 'post',
				url: '<?= site_url('quiz/save/'.$course.'/'.$topic.'/'.$quizfile); ?>',
				data: {quiz:quiz},
				success: function(d){
					console.log(d);
					var res = JSON.parse(d);
					toastr.success(res.message,res.status);
				}
			})
		})

		$('#save-question').on('click', function(){
			var question = mdquestion.value();
			var answer = $('input[name^=answer]').map(function(idx, elem) {
				return $(elem).prop('checked');
			}).get();
			var options = $('textarea[name^=options]').map(function(idx, elem) {
				return $(elem).val();
			}).get();
			console.log(question);
			console.log(answer);
			console.log(options);
			var questiontpl = `
			<div class="panel panel-default question" data-id="${qtotal}" data-title="" data-content="${btoa(question)}">
				<button type="button" class="btn btn-sm edit-question" id="0">edit</button>
				<div class="panel-body">
					<div class="question-content">
						${question}
					</div>
					<ul class="list-unstyled">`;
			var q = {
				question_title: "",
				question_content: btoa(question),
				options: []
			};

			options.forEach(function(item,idx,arr){
				var opt = `
				<li class="radio" data-isright="${answer[idx]}" data-content="${btoa(item)}">
					<label class="option-label ${answer[idx] ? 'isright': ''}">
						<input type="radio" name="${qtotal}">
						${item}								
					</label>
				</li>`;
				questiontpl += opt;

				var o = {
					is_right: answer[idx] ? 1 : 0,
					option_content: btoa(item),
				};		
				q.options.push(o);
			});
			questiontpl += `</ul></div></div>`;
			questions.push(q);
			qtotal++;

			$('#quizzes').append(questiontpl);
			mdquestion.value("");
			$('.option-form').empty();
			$('#add-option').click();
			$('#close-form').click();
			$('#save-quiz').click();
		})
	})
</script>