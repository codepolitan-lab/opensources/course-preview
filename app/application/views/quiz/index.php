<style>ul {font-size: 16px;}</style>
<div class="container">
	<h1>Course: <?= $this->uri->segment(3); ?></h1><br>
	<hr>
	<ul class="list-unstyled">
		<?php foreach($quiz as $topic => $quizzes): ?>
			<li style="padding-bottom: 10px">
				<h3><?= rtrim($topic,'/'); ?></h3>
				<?php $i = 1; if(!empty($quizzes)): ?>
					<ul style="margin-bottom:10px">
						<?php foreach($quizzes as $q): $i++; ?>
							<li>
								<?= $q; ?> &middot;
								<small><a href="<?= site_url('quiz/editor/'.$course.'/'.$topic.$q); ?>">Edit</a></small>
							</li>
						<?php endforeach; ?>
					</ul>
				<?php else: ?>
					<p><em class="text-muted">No quiz</em></p>
				<?php endif; ?>

				<?php
				$topicslug = rtrim(strtolower(substr($topic, 3)), '/');
				$filename = '0'.$i.'-quiz-'.$topicslug.'.yml';
				?>

				<a href="<?= site_url('quiz/editor/'.$course.'/'.$topic.$filename); ?>">Create Quiz</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>