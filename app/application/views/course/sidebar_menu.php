<?php foreach ($tree as $topictree): ?>
    <li>
        <label><?=$topictree['title'];?></label>
        <ul class="list-unstyled">
        <?php foreach ($topictree['lessons'] as $lessontree): ?>
            <li>
                <a href="<?= $topictree['folder'] . $lessontree['number'] . '-' . $lessontree['slug'] . '.html';?>"><?=$lessontree['title'];?></a>
            </li>
        <?php endforeach;?>
        </ul>
    </li>
<?php endforeach;?>