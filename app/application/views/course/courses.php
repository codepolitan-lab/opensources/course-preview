<div class="container">
	
	<h1>Courses</h1>
	<hr>
	<ul class="list-unstyled">
		<?php foreach ($courses as $course): ?>
			<li>
				<h4>
				<a href="<?php echo site_url('course/'.$course); ?>">
					<?= trim($course, '/'); ?>	
				</a> &middot;
				<small><a href="<?= site_url('quiz/index/'.$course); ?>">quiz</a></small>
			</h4>
			</li>
		<?php endforeach ?>
	</ul>

</div>
