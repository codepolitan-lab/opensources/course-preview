<!-- SECTION LESSON CONTAINER -->
<section class="lesson-container">

	<div id="wrapper">
		<a href="#menu-toggle" class="btn btn-lg" id="menu-toggle">
			<span class="glyphicon glyphicon-menu-hamburger"></span>
		</a>

		<div id="sidebar-wrapper">
			<ul class="sidebar-nav">
				<?php foreach ($lesson_tree as $topictree): ?>
					<li>
						<label><?= $topictree['title']; ?></label>
						<ul class="list-unstyled">					
							<?php foreach ($topictree['lessons'] as $lessontree): ?>
								<li>
									<a class="<?php echo $lesson['slug'] == $lessontree['number'].'-'.$lessontree['slug'] ? 'active' : ''; ?>" href="<?= site_url('course/'.$course.'/'.$topictree['folder'].$lessontree['number'].'-'.$lessontree['slug']); ?>"><?= $lessontree['title']; ?></a>
								</li>
							<?php endforeach; ?>
						</ul>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<!-- /#sidebar-wrapper -->

	<div class="container-fluid">
		<div class="row">

			<div class="col-md-7 col-md-offset-3">
				<div class="content-wrapper">
					<div class="content-header">
						<h1 class="content-title"><?= $lesson['title']; ?></h1>
					</div>

					<div class="content-body">
						<?php if (isset($lesson['video'])): ?>
							<!-- VIDEO PLAYER -->
							<div class="video-content-wrapper">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $lesson['video']; ?>?modestbranding=1&rel=0&iv_load_policy=3&enablejsapi=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
								</div>
							</div>
							<hr>
							<!-- END: VIDEO PLAYER -->
						<?php endif;?>

						<?= $lesson['content']; ?>
					</div>

				</div>
			</div>

		</div>
	</div>

</section>
<!-- END: SECTION LESSON CONTAINER