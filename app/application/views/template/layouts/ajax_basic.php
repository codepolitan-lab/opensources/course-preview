<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<?php $this->load->view('template/partials/head'); ?>
</head>
<body>
	<?php $this->load->view('template/partials/navbar'); ?>

	<?php echo $body; ?>

	<?php $this->load->view('template/partials/new_footer'); ?>
</body>
</html>
