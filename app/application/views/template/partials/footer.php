<!-- Bridge -->
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">

<!-- Jquery Head -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>

<!-- put this runner code before prism or any syntax highlighting plugin -->
<?php if(! isset($print)): ?>
	<script id="runnerUrl" data-url="https://www.codepolitan.com/" src="https://www.codepolitan.com/themes/belajarcoding/assets/js/cp_runner.js"></script>
<?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/prism.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/components/prism-php.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/components/prism-java.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.9.0/components/prism-python.min.js"></script>

<script>
	$(".btn-scroll-to").click(function() {
		var href_data = $(this).attr("href");

		$('html, body').animate({
			scrollTop: $(href_data).offset().top - 46
		}, 1000);

		return false;
	});

	$('form').submit(function(e){
		$(this).find('button[type=submit]').prop( "disabled", true ).html('<span class="fa fa-spinner rotate"></span> Please wait..');
	});


  	/*
	$('#general_popup').modal('show');
	*/

	$("iframe").load(function() {
		$(this).height( $(this).contents().find("body").height() );
	});

	var menu_open = Cookies.get('menu_open');
	if(menu_open == 'opened'){
		$("#wrapper").addClass("toggled");
		var activeScroll = $("#sidebar-wrapper a.active").offset().top - $("#sidebar-wrapper a.active").offsetParent().offset().top - 80;
		$("#sidebar-wrapper").scrollTop(activeScroll);
	}

	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("toggled");

		if(menu_open == 'opened')
			Cookies.set('menu_open', 'closed');
		else
			Cookies.set('menu_open', 'opened');
	});
</script>
