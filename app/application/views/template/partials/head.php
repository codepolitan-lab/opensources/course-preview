<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Kresna Galuh D. Herlangga">
<title><?php echo $title; ?></title>

<!-- Yoast -->
<link rel="canonical" href="<?php echo current_url(); ?>">
<meta property="og:locale" content="id_ID">
<meta property="og:type" content="article">
<meta property="og:title" content="<?php echo $title; ?>">
<meta property="og:url" content="<?php echo current_url(); ?>">
<meta property="og:site_name" content="CodePolitan.com">
<meta property="article:publisher" content="https://www.facebook.com/codepolitan">

<meta property="og:image:width" content="700">
<meta property="og:image:height" content="350">
<!-- / Yoast SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="<?php echo $title; ?>" href="https://school.codepolitan.com/feed/">
<meta property="og:title" content="<?php echo $title; ?>">
<meta property="og:type" content="article">
<meta property="og:site_name" content="CodePolitan.com">
<meta property="language" content="Indonesia">
<meta property="revisit-after" content="7">
<meta property="webcrawlers" content="all">
<meta property="rating" content="general">
<meta property="spiders" content="all">
<meta property="robots" content="all">

<!-- STYLESHEET -->
<link href="https://fonts.googleapis.com/css?family=Lora:400,700|Open+Sans:400,600,700,800|Roboto:300,400,500,700,900" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/black/pace-theme-minimal.min.css" rel="stylesheet" type="text/css">

<!-- PRISM FOR SYNTAX HIGHLIGHTING -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.12.2/themes/prism-okaidia.min.css" rel="stylesheet" type="text/css">

<!-- CUSTOM CSS -->
<link href="<?php echo base_url(); ?>assets/css/content.css" rel="stylesheet" type="text/css">
<?php if(isset($print)): ?>
<link href="<?php echo base_url(); ?>assets/css/print.css" rel="stylesheet" type="text/css">
<?php endif; ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
