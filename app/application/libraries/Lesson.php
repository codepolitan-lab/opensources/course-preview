<?php

class Lesson {

	public function _lesson_tree($courses_path)
	{
		$folders = $this->_get_file_tree($courses_path);
		$topics = [];
		foreach ($folders as $folder)
		{
			list($number, $slug) = explode('-', trim($folder,'/'), 2);
			$title = str_replace("-", " ", $slug);

			// prepare lesson list first
			$lessons = [];
			$lesson_folders = $this->_get_file_tree($courses_path.$folder);
			foreach ($lesson_folders as $lesson_file)
			{
				list($lesson_number, $lesson_slug) = explode('-', trim($lesson_file,'/'), 2);
				$lesson_slug = str_replace('.md', "", $lesson_slug);
				$lesson_title = str_replace("-", " ", $lesson_slug);

				// get slug and name
				$lessons[$lesson_number.'-'.$lesson_slug] = [
					'file'	=> $lesson_file,
					'number'=> $lesson_number,
					'slug' 	=> $lesson_slug,
					'title' => $lesson_slug == strtolower($lesson_slug) 
								? ucwords($lesson_title) 
								: $lesson_title
				];
			}

			// compact lesson to topic
			$topics[trim($folder, '/')] = [
				'folder'	=> $folder,
				'number'	=> $number,
				'slug' 		=> $slug,
				'title' 	=> $slug == strtolower($slug) ? ucwords($title) : $title,
				'lessons' 	=> $lessons
			];
		}

		return $topics;
	}

	public function get_course_quiz($course_path)
	{
		$folders = $this->_get_file_tree($course_path);

		$quiz = [];
		if ($folders) {
			foreach ($folders as $topic) {
				$quiz[$topic] = $this->get_quiz_topic($course_path, $topic);
			}
		}
		return $quiz;
	}
	
	public function get_quiz_topic($course_path, $topic = false)
	{
		$lessons = $this->_get_file_tree($course_path.$topic);

		if($lessons){
			$quiz = array_filter($lessons, function($v){
				return preg_match('/[0-9]+-quiz-/', $v);
			});
			return $quiz;
		}

		return [];
	}

	public function _get_file_tree($path)
	{
		$folders = directory_map($path, 1);
		
		if($folders){
			sort($folders);
			$folders = array_filter($folders, function($v){
				return preg_match('/[0-9]+-/', $v);
			});
		}
		
		return $folders;
	}

}