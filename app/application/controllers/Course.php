<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Symfony\Component\Yaml\Yaml;

class Course extends CI_Controller {

	public $courses_path = "";
	public $course_folder = "courses/";

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');

		define('VERSION', file_get_contents('version'));

		if($this->session->userdata('course'))
			$this->courses_path = $this->course_folder.$this->session->userdata('course');
	}

	function index($course = false)
	{
		if($course){
			redirect('course/lesson/'.$course);
		}

		// get course topics folder list
		$folders = array_filter(directory_map($this->course_folder, 1), function($var){
			return file_exists($this->course_folder.$var.'course.yml');
		});

		$data['title'] = 'Choose Course';
		$data['courses'] = $folders;
		$this->render('course/courses', $data);
	}

	/*
	 * Show lesson list
	 */
	function lesson($course = false, $topic = false, $lessonSlug = false)
	{
		if(! $course) redirect('course/choose_course');
		
		$this->courses_path = $this->course_folder.$course.'/';

		$this->load->library('lesson');
		$lesson_tree = $this->lesson->_lesson_tree($this->courses_path);
		
		// get first topic
		if(! $topic) {
			reset($lesson_tree);
			$topic = key($lesson_tree);
		}

		// prepare lesson content and attributes
		// if lessonSlug not defined, choose first file
		reset($lesson_tree[$topic]['lessons']);
		if(!$lessonSlug) $lessonSlug = key($lesson_tree[$topic]['lessons']);

		// get file content
		$content = file_get_contents($this->courses_path.$topic.'/'.$lessonSlug.'.md');

		// separate yaml and content
		if(strpos($content, '---') === false)
			show_error('Lesson attribute separator (---) not found in '.$this->courses_path.$topic.'/'.$lessonSlug.'.md');
			
		list($attributes, $content) = explode("---", $content, 2);
		
		$currentLesson = Yaml::parse($attributes);
		$currentLesson['slug'] = $lessonSlug;

		// else {
		// 	// at least get first row as title
		// 	list($attributes, $content) = explode(PHP_EOL, $content, 2);
		// 	$currentLesson['title'] = str_replace('#','', $attributes);
		// 	$currentLesson['slug'] = $lessonSlug;
		// }

		// parse content
		$Parsedown = new ParsedownExtra();
		$currentLesson['content'] = $Parsedown->text($content);

		// change images path
		$pattern = '../assets/images/';
		$replace = site_url($this->courses_path.'assets/images/');
		$currentLesson['content'] = str_replace($pattern, $replace, $currentLesson['content']);

		// load view
		$data['title'] =  $currentLesson['title'];
		$data['lesson_tree'] = $lesson_tree;
		$data['course'] = $course;
		$data['topic'] = $lesson_tree[$topic];
		$data['lessons'] = $lesson_tree[$topic]['lessons'];
		$data['lesson'] = $currentLesson;

		$this->render('course/lesson', $data);
	}

	/*
	 * Show lesson list
	 */
	function printing($topic = false)
	{
		if(empty($this->courses_path))
			redirect('course/choose_course');

		// get lesson files list
		chdir($this->courses_path.$topic);
		$files = glob('??-*');
		if(empty($files)) show_error('Topic must have at least one lesson file and named such as 01-some-title-for-your-lesson.md');
		// print_r($files);
		
		// prepare lesson list
		$lessons = [];
		$content = '';
		foreach ($files as $file){
			// separate number and name
			list($number, $slug) = explode('-', $file, 2);

			$title = str_replace(["-",".md"], [" ",""], $slug);

			// get slug and name
			$lessons = [
				'file' => $file,
				'number' => $number,
				'slug' => str_replace(".md", "", $slug),
				'title' => $slug == strtolower($slug) ? ucwords($title) : $title
			];

			$content .= file_get_contents($file).PHP_EOL;
		}
		// print_r($lessons);

		// parse content
		$Parsedown = new ParsedownExtra();
		$lesson['content'] = $Parsedown->text($content);

		// change images path
		$pattern = '../assets/images/';
		$replace = site_url($this->courses_path.'assets/images/');
		$lesson['content'] = str_replace($pattern, $replace, $lesson['content']);

		// set topic attributes
		list($number, $topicSlug) = explode('-', $topic, 2);
		$topic = [
			'folder' => $topic,
			'slug' => $topicSlug,
			'title' => str_replace("-", " ", $topicSlug)
		];
		if($topicSlug == strtolower($topicSlug))
			$topic['title'] = ucwords($topic['title']);

		$lesson['title'] = $topic['title'];

		// load view
		$data['title'] =  $lesson['title'];
		$data['lessons'] = $lessons;
		$data['lesson'] = $lesson;
		$data['topic'] = $topic;
		$data['print'] = true;
		$this->render('course/lesson', $data);
	}

	function render($template = 'page', $data)
	{
		$data['body'] = $this->load->view($template, $data, true);
		$this->load->view('template/layouts/basic', $data);
	}

}
