<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Symfony\Component\Yaml\Yaml;

class Quiz extends CI_Controller {

	public $courses_path = "";
	public $course_folder = "courses/";

	function __construct()
	{
		parent::__construct();
	}

	function index($course = false)
	{
		$course or show_error('Course slug undefined, '.site_url('quiz/index/[course-slug]').'<br><a href="'.site_url('course').'">back to courses</a>');

		$this->load->library('lesson');
		$data['quiz'] = $this->lesson->get_course_quiz($this->course_folder.$course.'/');

		$data['title'] = 'Quiz';
		$data['course'] = $course;
		$this->render('quiz/index', $data);
	}

	function editor($course = false, $topic = false, $quiz = null)
	{
		$course or show_error('Course slug not defined');
		$topic or show_error('Course topic not defined');
		if(! file_exists($this->course_folder.$course.'/')) show_error('Course not found');
		if(! file_exists($this->course_folder.$course.'/'.$topic)) show_error('Topic not found');

		// if edit
		if(file_exists($this->course_folder.$course.'/'.$topic.'/'.$quiz)) {
			$content = file_get_contents($this->course_folder.$course.'/'.$topic.'/'.$quiz);
			$data['quiz'] = Yaml::parse($content);
			$data['quiz']['file'] = $quiz;
		} else {
			$data['quiz']['title'] = 'quiz-'.strtolower(substr($topic, 3));
			write_file($this->course_folder.$course.'/'.$topic.'/'.$quiz, '');
		}

		$data['title'] = 'Quiz Editor';
		$data['course'] = $course;
		$data['topic'] = $topic;
		$data['parsedown'] = new ParsedownExtra();
		$data['quizfile'] = $quiz;
		$this->render('quiz/editor', $data);
	}

	function save($course, $topic, $quiz)
	{
		$file = $this->course_folder.$course.'/'.$topic.'/'.$quiz;
		if(!file_exists($file)){
			echo json_encode(['status'=>'fail','message'=>'file not found']);
			return;
		}

		$data = $this->input->post('quiz');
		$data['random_question'] = (bool) $data['random_question'];
		$data['random_answer'] = (bool) $data['random_answer'];
		if(isset($data['questions'])){
			foreach ($data['questions'] as &$question) {
				$question['question_content'] = base64_decode($question['question_content']);
				foreach ($question['options'] as &$option) {
					$option['is_right'] = (bool) $option['is_right'];
					$option['option_content'] = base64_decode($option['option_content']);
				}
			}
		}
		$yaml = Yaml::dump($data, 5, 2, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
		write_file($file, $yaml);
		echo json_encode(['status'=>'success','message'=>'Quiz Saved']);
	}

	function render($template = 'page', $data = [])
	{
		$data['title'] = $data['title'] ?? '';
		$data['body'] = $this->load->view($template, $data, true);
		$this->load->view('template/layouts/basic', $data);
	}

}
