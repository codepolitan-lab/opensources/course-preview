$.ajaxLoad = true;
$.defaultPage = 'views/main.html';
$.subPagesDirectory = 'views/';
$.page404 = 'views/404.html';
$.mainContent = $('#main-content');
$.navigation = $('ul.sidebar-nav');
$.panelIconOpened = 'icon-arrow-up';
$.panelIconClosed = 'icon-arrow-down';
$.brandPrimary = '#20a8d8';
$.brandSuccess = '#4dbd74';
$.brandInfo = '#63c2de';
$.brandWarning = '#f8cb00';
$.brandDanger = '#f86c6b';
$.grayDark = '#2a2c36';
$.gray = '#55595c';
$.grayLight = '#818a91';
$.grayLighter = '#d1d4d7';
$.grayLightest = '#f8f9fa';
'use strict';


if ($.ajaxLoad) {
    var paceOptions = {
        elements: false,
        restartOnRequestAfter: false
    };
    var url = location.hash.replace(/^#/, '');
    if (url != '') {
        setUpUrl(url);
    } else {
        setUpUrl($.defaultPage);
    }
    $(document).on('click', '.sidebar-nav a[href!="#"]', function(e) {
        var thisNav = $(this).parent().parent();
        if (thisNav.hasClass('nav-tabs') || thisNav.hasClass('nav-pills')) {
            e.preventDefault();
        } else if ($(this).attr('target') == '_top') {
            e.preventDefault();
            var target = $(e.currentTarget);
            window.location = (target.attr('href'));
        } else if ($(this).attr('target') == '_blank') {
            e.preventDefault();
            var target = $(e.currentTarget);
            window.open(target.attr('href'));
        } else {
            e.preventDefault();
            var target = $(e.currentTarget);
            setUpUrl(target.attr('href'));
        }
    });
    $(document).on('click', 'a[href="#"]', function(e) {
        e.preventDefault();
    });
    $(document).on('click', '.sidebar .nav a[href!="#"]', function(e) {
        if (document.body.classList.contains('sidebar-mobile-show')) {
            document.body.classList.toggle('sidebar-mobile-show')
        }
    });
}

function setUpUrl(url) {
    console.log(url);
    $('.sidebar-nav').find('a').removeClass('active');
    $('.sidebar-nav').find('a[href="' + url + '"]').addClass('active');
    loadPage(url);
}

function loadPage(url) {
    $.ajax({
        type: 'GET',
        url: $.subPagesDirectory + url,
        dataType: 'html',
        cache: false,
        async: false,
        beforeSend: function() {
            $.mainContent.css({
                opacity: 0
            });
        },
        success: function() {
            Pace.restart();
            $('html, body').animate({
                scrollTop: 0
            }, 0);
            $.mainContent.load($.subPagesDirectory + url, null, function(responseText) {
                window.location.hash = url;
            }).delay(250).animate({
                opacity: 1
            }, 0);
        },
        // error: function() {
        //     window.location.href = $.page404;
        // }
    });
}

$(document).ready(function($) {
    // load menu content
    $('.sidebar-nav').load('assets/menu.html', null, function(responseText){
        $('.sidebar-nav').find('a').removeClass('active');
        $('.sidebar-nav').find('a[href="' + url + '"]').addClass('active');
        var activeScroll = $("#sidebar-wrapper a.active").offset().top - $("#sidebar-wrapper a.active").offsetParent().offset().top - 80;
        $("#sidebar-wrapper").scrollTop(activeScroll);
    });

    $("iframe").load(function () {
        $(this).height($(this).contents().find("body").height());
    });
    
    // show or hide sidebar menu
    var menu_open = Cookies.get('menu_open');
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        
        if (menu_open == 'opened')
            Cookies.set('menu_open', 'closed');
        else
            Cookies.set('menu_open', 'opened');
    });
    if (menu_open == 'opened') {
        $("#wrapper").addClass("toggled");
    }

    
    $.navigation.find('a').each(function() {
        var cUrl = String(window.location).split('?')[0];
        if (cUrl.substr(cUrl.length - 1) == '#') {
            cUrl = cUrl.slice(0, -1);
        }
        if ($($(this))[0].href == cUrl) {
            $(this).addClass('active');
            $(this).parents('ul').add(this).each(function() {
                $(this).parent().addClass('open');
            });
        }
    });
    $.navigation.on('click', 'a', function(e) {
        if ($.ajaxLoad) {
            e.preventDefault();
        }
        if ($(this).hasClass('nav-dropdown-toggle')) {
            $(this).parent().toggleClass('open');
            resizeBroadcast();
        }
    });
});

function init(url) {
    $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({
        "placement": "bottom",
        delay: {
            show: 400,
            hide: 200
        }
    });
    $('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();
}
if (!Array.from) {
    Array.from = (function() {
        var toStr = Object.prototype.toString;
        var isCallable = function(fn) {
            return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
        };
        var toInteger = function(value) {
            var number = Number(value);
            if (isNaN(number)) {
                return 0;
            }
            if (number === 0 || !isFinite(number)) {
                return number;
            }
            return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
        };
        var maxSafeInteger = Math.pow(2, 53) - 1;
        var toLength = function(value) {
            var len = toInteger(value);
            return Math.min(Math.max(len, 0), maxSafeInteger);
        };
        return function from(arrayLike) {
            var C = this;
            var items = Object(arrayLike);
            if (arrayLike == null) {
                throw new TypeError('Array.from requires an array-like object - not null or undefined');
            }
            var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
            var T;
            if (typeof mapFn !== 'undefined') {
                if (!isCallable(mapFn)) {
                    throw new TypeError('Array.from: when provided, the second argument must be a function');
                }
                if (arguments.length > 2) {
                    T = arguments[2];
                }
            }
            var len = toLength(items.length);
            var A = isCallable(C) ? Object(new C(len)) : new Array(len);
            var k = 0;
            var kValue;
            while (k < len) {
                kValue = items[k];
                if (mapFn) {
                    A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
                } else {
                    A[k] = kValue;
                }
                k += 1;
            }
            A.length = len;
            return A;
        };
    }());
}
if (!Array.prototype.includes) {
    Object.defineProperty(Array.prototype, 'includes', {
        value: function(searchElement, fromIndex) {
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }
            var o = Object(this);
            var len = o.length >>> 0;
            if (len === 0) {
                return false;
            }
            var n = fromIndex | 0;
            var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

            function sameValueZero(x, y) {
                return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
            }
            while (k < len) {
                if (sameValueZero(o[k], searchElement)) {
                    return true;
                }
                k++;
            }
            return false;
        }
    });
}