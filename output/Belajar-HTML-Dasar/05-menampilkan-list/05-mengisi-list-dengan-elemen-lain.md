## Mengisi List dengan Elemen Lain

Kita tidak hanya dapat mengisi konten list dengan teks, tapi juga dengan elemen lain, seperti elemen gambar maupun tautan link.

```html runner-html
<ul style="list-style: none;">
	<li>
        <img src="https://static.cdn-cdpl.com/270x135/lock.jpg" />
    </li>
	<li>
        <img src="https://static.cdn-cdpl.com/270x135/xampp.jpg" />
    </li>
	<li>
        <img src="https://static.cdn-cdpl.com/270x135/Java.jpg" />
    </li>
</ul>
```







