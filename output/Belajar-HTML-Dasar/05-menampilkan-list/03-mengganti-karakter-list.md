## Mengganti Karakter List

Pada *ordered list* kita dapat menggunakan tidak hanya list angka saja (1, 2, 3, dst) tapi juga huruf (a, b, c, dst) dan angka romawi (I, II, III, dst). Untuk mengubah tipe *ordered list*, kita gunakan atribut `type` di dalam tag `<ol>`.

Adapun tipe yang dapat digunakan untuk *ordered list* diantaranya:

| Tipe     | Deskripsi                                |
| -------- | ---------------------------------------- |
| type="1" | Item list menggunakan angka (default)    |
| type="A" | Item list menggunakan huruf kapital      |
| type="a" | Item list menggunakan huruf kecil        |
| type="I" | Item list menggunakan angka romawi kapital |
| type="i" | Item list menggunakan angka romawi kecil |


Adapun untuk *unordered list*, simbol bawaannya adalah lingkaran hitam. Kita dapat mengganti simbol tersebut menggunakan atribut `type` pada tag `<ul>`.

Adapun tipe yang dapat kita gunakan untuk *unordered list* diantaranya:

| Nilai  | Deskripsi                                |
| ------ | ---------------------------------------- |
| disc   | Item list menggunakan simbol lingkaran (default) |
| circle | Item list menggunakan simbol garis luar lingkaran |
| square | Item list menggunakan simbol kotak       |
| none   | Item list tidak menggunakan simbol list  |

```html runner-html
<ol type="A">
	<li>List Pertama</li>
	<li>List Kedua</li>
	<li>List Ketiga</li>
</ol>

<ul style="list-style-type:circle">
	<li>List Pertama</li>
	<li>List Kedua</li>
	<li>List Ketiga</li>
</ul>

```

