## Struktur Form

Form adalah salah satu elemen HTML yang sangat sering dijumpai bagi Kamu pengguna layanan internet. Hampir seluruh halaman web yang Kamu kunjungi mengandung unsur form di dalamnya. Contoh paling sederhana adalah form login dan register.

Sebuah form dalam HTML harus berada di dalam tag form, yang diawali dengan `<form>` dan diakhiri dengan `</form>`. Struktur dasar form akan terlihat sebagai berikut:

```html
<form action="aksi.php" method="POST">
	<!-- isi form -->
</form>
```

Tag form akan membutuhkan beberapa atribut untuk dapat berfungsi dengan seharusnya. Di dalam elemen form seperti contoh di atas, terdapat atribut-atribut yang penting antara lain:

- **Action**, atribut ini menentukan alamat pengiriman data input pengguna. Jika kosong, berarti form dikirimkan ke server dengan alamat yang sama dengan alamat halaman tersebut.
* **Method**, merupakan metode pengiriman data. Form dapat mengirimkan data menggunakan dua metode rekues HTTP (*HTTP request method*), yakni `GET` dan `POST`. Lebih jauh cara penggunaan method ini akan Kamu jumpai di pembahasan bahasa pemrograman server side seperti PHP dan Python.



