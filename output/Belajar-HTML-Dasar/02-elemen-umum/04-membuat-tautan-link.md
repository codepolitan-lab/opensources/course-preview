## Membuat Tautan (Link)

Halaman website dapat berisi berbagai tautan yang membawa kita langsung ke halaman lain atau bagian tertentu dari sebuah halaman. Tautan ini dikenal sebagai *hyperlink*. *Hyperlink* pada html adalah suatu cara untuk menghubungkan dokumen satu dengan yang lainnya.

Tautan dibuat menggunakan tag HTML `<a>`. Tag ini disebut tag *anchor* dan apapun di antara tag `<a>` pembuka dan tag penutup `</a>` menjadi bagian dari tautan dan pengguna dapat mengklik bagian tersebut untuk menjangkau dokumen yang ditautkan. 

```html
<a href="url">teks link</a>
```

Tag `<a>` menggunakan atribut `href` yang berisi alamat yang dituju. *href* adalah singkatan dari *hypertext reference*. Teks link pada contoh di atas adalah teks yang akan kita jadikan link. Ketika teks link tersebut diklik, maka halaman akan berpindah ke alamat tujuan link.

Kita tidak hanya dapat menggunakan teks sebagai link, tapi juga gambar atau elemen lainnya.

```html
<a href="url"><img src="gambar.jpg" /></a>
```

Coba jalankan kode di bawah ini:

```html runner-html
<!DOCTYPE html>
<html>
	<head>
		<title>Belajar HTML</title>
	</head>
	<body>
      <p>Contoh link dengan teks:</p>
      <a href="https://www.codepolitan.com" target="_blank">CodePolitan.com</a>
      
      <p>Contoh link dengan gambar:</p>
      <a href="https://www.codepolitan.com" target="_blank">
        <img src="https://i.imgur.com/urfCJFH.png" />
      </a>
	</body>
</html>
```

### Atribut Target

Atribut `target` kita gunakan untuk menentukan dimana alamat link harus dibuka.

- `_self` - Membuka alamat link di tab/jendela browser yang sama (nilai bawaan)
- `_blank` - Membuka alamat link di tab/jendela browser yang baru
- `_parent` - Membuka alamat link di frame induk
- `_top` - Membuka alamat link di jendela penuh
- `framename` - Membuka alamat link di frame yang namanya sudah ditentukan

Umumnya kita akan sering menggunakan nilai `_blank` untuk menampilkan halaman di tab browser baru. Bila atribut ini tidak dipasang, maka nilai defaultnya adalah `_self`, artinya alamat link tersebut akan dibuka di jendela yang sedang aktif.