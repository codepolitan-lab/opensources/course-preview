## Menampilkan Blok Kode

Sebagai programmer tentu Kamu akan sering bersentuhan dengan kode program. Saat Kamu membuat tulisan tentang kode programmu, Kamu akan perlu menampilkan kode agar tampilannya lebih mudah dibaca. Daripada seperti ini:

var x = 5;
var y = 6;
alert(x + y);

Tentu akan lebih mudah dan nyaman dibaca bila ditulis seperti ini:

``` 
var x = 5;
var y = 6;
alert(x + y);
```

Untuk menuliskan kode di dalam HTML, kita dapat menggunakan elemen `<code></code>` atau `<pre></pre>` atau kombinasi keduanya. Perbedaannya hanya terdapat pada format penulisan. 

Elemen `<code></code>` bersifat *inline* (dalam satu baris). Walaupun kita menulis kode dalam banyak baris, hasilnya akan tetap ditampilkan dalam satu baris. 

Sedangkan elemen `<pre></pre>` akan menampilkan teks apa adanya sesuai dengan apa yang ditulis pada dokumen HTML, termasuk baris baru (*newline*) dan sejumlah spasi beruntun.

```html runner-html
<p>Contoh penggunaan tag code</p>
<code>
var x = 5;
var y = 6;
alert(x + y);
</code>

<p>contoh penggunaan tag code di dalam tag pre </p>
<pre>
<code>
var x = 5;
var y = 6;
alert(x + y);
</code>
</pre>
```

