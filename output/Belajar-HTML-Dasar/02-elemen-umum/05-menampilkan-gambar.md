## Menampilkan Gambar

Kita dapat menampilkan gambar pada HTML dengan elemen `<img>`. Elemen ini tidak memiliki konten, hanya menggunakan atribut saja dan tidak memilik tag penutup.

```html
<img src="gambar.jpg" />
```

Atribut `src` digunakan untuk menentukan alamat gambar yang hendak ditampilkan. Atribut ini diisi dengan path dimana gambar disimpan. Penulisan lokasi file gambar dihitung dari tempat dimana dokumen htmlnya disimpan. Bila gambar disimpan di dalam folder yang sama dengan file htmlnya, maka kita tinggal menulis nama filenya saja. Bila kita menyimpan file gambar di dalam folder, maka kita tuliskan juga nama foldernya diikuti tanda garis miring.

Misalkan bila kita menyimpan file dokumen html dan file gambar dengan struktur foldet seperti ini:

```
myweb/
+-- index.html
+-- img/
    +-- gambar.jpg
```

maka pada file `index.html` kita panggil gambar `cover.jpg` seperti ini:

```html
<img src="img/gambar.jpg" />
```

Adapun bila file gambar berada di dalam folder di luar folder dimana file html disimpan, seperti ini:

 ```
myweb/
+-- www/
    +-- index.html
+-- img/
    +-- gambar.jpg
 ```

maka pada file `index.html ` kita arahkan lokasi gambar keluar dari folder dimana file `index.html` disimpan, baru kemudian arahkan ke dalam folder gambarnya:

```html
<img src="../img/gambar.jpg" />
```

tanda `../` artinya kita naik satu tingkat ke folder induknya. Pada contoh kasus di atas, folder `../` artinya masuk ke folder `myweb/`.

Kita dapat menggunakan file gambar PNG, JPEG atau GIF. Namun, pastikan kita menentukan nama file gambar yang benar di atribut `src`. Pemanggilan nama file dan folder bersifat _case sensitive_ (huruf kecil dan kapital dianggap berbeda).

### Mengatur Ukuran Gambar

Kita juga dapat mengatur ukuran gambar menggunakan atribut *width* untuk panjang gambar dan *height* untuk tinggi gambar.

```html
<img src="../img/gambar.jpg" width="100%" height="20px" />
```

Untuk memastikan rasio panjang dan lebar gambar tetap konsisten dengan ukuran aslinya, kita cukup mengeset hanya atribut `width` saja atau `height` saja.

### Memanggil Gambar dari Internet

Kita juga dapat memasukkan gambar menggunakan lokasi gambar yang ada di internet, dengan memasukkan URL dari gambarnya.

```html runner-html
<img src = "http://i.imgur.com/cXlXzVg.jpg" />
```

