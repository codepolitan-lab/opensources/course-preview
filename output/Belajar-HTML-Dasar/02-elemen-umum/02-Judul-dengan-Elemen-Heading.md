## Judul dengan Elemen Heading

*Headings* adalah elemen HTML yang digunakan untuk menampilkan judul dari sebuah konten. Elemen heading ada 6 tingkatan, yakni `<h1>`, `<h2>`, `<h3>`, `<h4>`, `<h5>` dan `<h6>` yang merepresentasikan prioritas judul. `<h1>` akan menampilkan teks dengan ukurang lebih besar daripada `<h2>`. `<h2>` juga akan menampilkan teks dengan ukuran lebih besar dari `<h3>`. Begitu seterusnya.

```html
<h1>Ini heading 1</h1>
<h2>Ini heading 2</h2>
<h3>Ini heading 3</h3>
<h4>Ini heading 4</h4>
<h5>Ini heading 5</h5>
<h6>Ini heading 6</h6>
```

Bila kita hendak membuat judul sebuah konten, maka gunakanlah `<h1>` sebagai prioritas utama. Bila hendak membuat subjudul di dalam konten, maka gunakan `<h2>`, dan bila hendak membuat sub-subjudul, maka gunakan `<h3>`, dan seterusnya.

```html runner-html
<!DOCTYPE html>
<html>
	<head>
		<title>Belajar HTML</title>
	</head>
	<body>
	    <h1>Judul Utama</h1>
      	<p>Paragraf pertama.</p>
        
        <h2>Subjudul</h2>
        <p>Paragraf kedua.</p>
	</body>
</html>
```

Disarankan untuk tidak melompati urutan tag judul (heading), jadi gunakan secara berurutan mulai `<h1>` ke `<h2>` dan seterusnya.