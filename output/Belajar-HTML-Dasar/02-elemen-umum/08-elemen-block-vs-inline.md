## Elemen Block vs Inline

Elemen di dalam HTML dapat dibagi menjadi 2 bagian, yakni berjenis `block` dan berjenis `inline`.

Elemen berjenis *inline* akan ‘*menyatu’* dengan tag sebelum dan sesudahnya. Elemen ini akan ditampilkan pada baris yang sama dengan elemen di sampingnya atau elemen induknya. Beberapa diantara elemen *inline* pada HTML diantaranya:
`<a>`, `<abbr>`, `<acronym>`, `<b>`, `<bdo>`, `<big>`, `<br>`, `<button>`, `<cite>`, `<code>`, `<dfn>`, `<em>`, `<i>`, `<img>`, `<input>`, `<kbd>`, `<label>`, `<map>`, `<object>`, `<q>`, `<samp>`, `<script>`, `<select>`, `<small>`, `<span>`, `<strong>`, `<sub>`, `<sup>`, `<textarea>`, `<time>`, `<tt>`, `<var>`

Elemen berjenis *block* akan ditampilkan dalam satu objek terpisah dalam satu baris mandiri. Ketika kita menuliskan beberapa elemen berjenis *block* ini dalam satu baris, masing-masing elemen tersebut akan ditampilkan dalam baris tersendiri pada browser. Beberapa diantara elemen *block* pada HTML diantaranya: `<address>`, `<article>`, `<aside>`, `<blockquote>`, `<canvas>`, `<dd>`, `<div>`, `<dl>`, `<dt>`, `<fieldset>`, `<figcaption>`, `<figure>`, `<footer>`, `<form>`, `<h1>` - `<h6>`, `<header>`, `<hr>`, `<li>`, `<main>`, `<nav>`, `<noscript>`, `<ol>`, `<output>`, `<p>`, `<pre>`, `<section>`, `<table>`, `<tfoot>`, `<ul>`, `<video>`.



```html runner-html
<!DOCTYPE html>
<html>
<head>
	<title>Belajar HTML</title>
</head>
<body>
    <h1>Elemen heading bersifat block</h1>
    <h2>Heading akan ditampilkan dalam baris mandiri</h2>
	<p>
        Paragraf ini berisi elemen <strong>strong</strong> 
        dan <em>emphasis</em> yang bersifat inline. 
        Elemen inline akan sesuai ukuran lebar kontennya.
    </p>
</body>
</html>
```