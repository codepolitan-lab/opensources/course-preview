## Blok Komentar pada HTML

Adakalanya kita harus memberikan penjelasan di dalam dokumen HTML tentang bagian-bagian tertentu di dalamnya, agar memudahkan kita atau orang lain untuk memahami isi dari dokumen HTML tersebut. Terlebih lagi bila HTML yang kita tulis sudah ratusan atau bahkan ribuan baris.

HTML menyediakan format untuk menulis komentar pada dokumen HTML. Blok komentar ini tidak akan ditampilkan di browser.

```
<!-- ini komentar -->
```

Komentar dibuka dengan tag `<!--` dan ditutup dengan tag `-->`. 

Tag komentar juga sangat berguna jika kita ingin mencoba menyembunyikan beberapa elemen pada dokumen, tetapi tidak ingin menghapus kode tersebut.

```html runner-html
<!DOCTYPE html>
<html>
<head>
	<title>Belajar HTML</title>
</head>
<body>
	<!-- ini gambar kucing -->
	<img src="http://i.imgur.com/cXlXzVg.jpg" />
</body>
</html>
```

