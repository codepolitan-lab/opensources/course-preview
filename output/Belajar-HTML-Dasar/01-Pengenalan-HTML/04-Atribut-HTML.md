## Atribut HTML
Atribut HTML adalah informasi tambahan yang diperlukan oleh sebuah elemen HTML.

Pada pembahasan sebelumnya kita sudah melihat contoh elemen `<img />`. Elemen ini memiliki teks tambahan di dalam tag pembukanya, yakni `src`.

```html
<img src="image.jpg" />
```

`src` yang terdapat di dalam tag `<img />` adalah salah satu contoh atribut HTML.

Atribut HTML selalu didefinisikan di dalam tag pembuka suatu elemen. Atribut biasanya ditulis dalam bentuk pasangan **nama** dan **nilai** atribut dengan format seperti ini: `nama="nilai"`.

Semua elemen HTML dapat mengandung atribut, baik itu atribut yang umum yang dapat dimiliki oleh semua elemen HTML, maupun atribut khusus untuk elemen HTML tertentu. Contoh atribut umum adalah `id` dan `class`. Contoh atribut khusus adalah `src` pada elemen `<img />` dan `href` pada elemen `<a>`.

```html
<img src="file-gambar.jpg" />
<a href="login.html"> Log In </a>
```

### Nama dan Nilai Atribut

Nama atribut dapat ditulis baik menggunakan huruf kapital maupun huruf kecil, akan tetapi sangat dianjurkan menggunakan huruf kecil semua sebagai sebuah standar sejak versi XHTML.

Nilai dari atribut HTML dapat ditulis tanpa menggunakan tanda kutip. Namun dalam banyak kasus tanda kutip harus digunakan untuk membungkus nilai atribut yang mengandung spasi.

```html
<h1 title=judul konten>Pendahuluan</h1>
```

Pada contoh di atas, atribut title bernilai 'judul' dan kata 'konten' akan dianggap sebagai sebuah atribut lain karena antara kata 'judul' dan 'konten' terpisah oleh spasi. Bila kita hendak mengisi nilai atribut title di atas dengan 'judul konten', maka kita harus menulisnya dengan tanda kutip:

```html
<h1 title="judul konten">Pendahuluan</h1>
```

### Tanda Kutip atau Petik?

Nilai atribut dapat menggunakan baik tanda kutip `"` maupun tanda petik `'`. Bila kita membuka dengan tanda kutip, maka tutup juga dengan tanda kutip. Begitu pula bila membuka dengan tanda petik, maka tutuplah dengan tanda petik.

Hal ini sangat berguna bila nilai atribut mengandung tanda kutip atau petik. Bila nilai atribut mengandung tanda kutip, maka gunakanlah petik untuk membungkus nilai atribut. Dan bila nilai atribut mengandung tanda petik, maka gunakanlah kutip untuk membungkus nilai atribut.

```html
<h1 title='Bosnya "Microsoft"'>Bill Gates</h1>
```

