## Layout Menggunakan Table

Layout halaman web sangat penting untuk memberikan tampilan yang lebih baik pada situs kita. Butuh banyak waktu untuk merancang tata letak situs web dengan tampilan dan nuansa yang bagus. Namun, kita dapat membuat layout halaman sederhana dengan menggunakan tag `<tabel>`. Tabel ini disusun dalam kolom dan baris, sehingga kita dapat memanfaatkan baris dan kolom ini dengan cara apa pun yang kita suka.

Berikut contoh membuat layout halaman menggunakan tabel:

```html runner-html
<table width="100%">
	<tr>
		<td colspan="2" style="background:red">
			<h1>Header</h1>
		</td>
	</tr>
	<tr valign="top">
		<td width="50" style="background:green">
			<b>Sidebar</b>
		</td>
		
		<td width="100" height="200" style="background:purple">
			<b>Konten Utama</b>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="background:blue">
			<b>Footer</b>
		</td>
	</tr> 
</table>
```

Seperti contoh di atas, kita membuat layout HTML menggunakan tabel dengan 3 baris dan 2 kolom. Namun kolom header dan footer menggunakan atribut colspan.

