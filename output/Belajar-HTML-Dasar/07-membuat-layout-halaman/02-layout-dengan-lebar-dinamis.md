## Layout dengan Lebar Dinamis

Untuk membuat layout dengan lebar dinamis, atur nilai atribut `width` pada tag `<td>` menggunakan nilai ukuran relatif/persentase (%). Berikut contoh layout dengan lebar dinamis:

```html runner-html
<table width="100%">
	<tr>
		<td colspan="2" style="background:red">
			<h1>Header</h1>
		</td>
	</tr>
	<tr valign="top">
		<td width="20%" style="background:green">
			<b>Sidebar</b>
		</td>
		
		<td width="80%" height="200" style="background:purple">
			<b>Konten Utama</b>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="background:blue">
			<b>Footer</b>
		</td>
	</tr> 
</table>
```

