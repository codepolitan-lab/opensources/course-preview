## Menampilkan Popup Peringatan

Untuk menerapkan kode JavaScript di dokumen HTML, kita gunakan elemen `<script>`. Kode JavaScipt disimpan di dalam elemen tersebut. Pada bagian ini kita akan belajar menerapkan kode JavaScript untuk menampilkan pesan *popup*.

```html
<script type="text/javascript">
	alert(“isi teks peringatan“);
</script>
```

Popup peringatan umumnya digunakan untuk menampilkan pesan peringatan kepada pengguna. Contohnya, jika satu field input mengharuskan untuk memasukkan beberapa teks, namun pengguna tidak memberikan masukan apapun, maka sebagai bagian dari validasi, kita dapat menggunakan pop up peringatan untuk memberi pesan peringatan.

Pop up peringatan hanya memberi satu tombol "OK" untuk memilih dan melanjutkan. Untuk membuat sebuah popup peringatan, kita tambahkan sintaks berikut di dalam function:

Berikut adalah contoh kode untuk menerapkan *popup* peringatan di HTML:

```html runner-html
<html>
<body>
	<p>Klik tombol untuk melihat hasilnya: </p>
	<button onclick="peringatan();">Klik saya</button>
    
    <script type="text/javascript">
		function peringatan() {
			alert ("Ini adalah pop up peringatan!");
		}
	</script>
</body>
</html>
```

Script popup di atas akan dieksekusi saat kita mengklik tombol, karena pada elemen `<button>` tersebut kita tambahkan atribut onclick yang akan memanggil fungsi `peringatan()` ketika tombol diklik.