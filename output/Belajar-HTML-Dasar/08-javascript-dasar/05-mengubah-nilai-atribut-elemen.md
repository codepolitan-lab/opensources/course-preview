## Mengubah Nilai Atribut Elemen

Selain merubah konten elemen, kita juga dapat merubah nilai dari suatu atribut. Untuk mengubah nilai dari suatu atribut, gunakan instaks berikut:

```javascript
document.getElementById(id).attribute = "nilai baru"
```

Misalnya kita ingin mengubah gambar dari elemen `<img>`, berarti kita harus mengubah nilai atribut `src` pada elemen tersebut:

```html runner-html
<!DOCTYPE html>
<html>
<body>
  <img id="myImage" 
       src="https://static.cdn-cdpl.com/340x195/programmer_wanita.jpg"><br>
  <button onclick="gantiGambar()">Ubah Gambar</button>

  <script>
    function gantiGambar(){
      var img = "https://static.cdn-cdpl.com/340x195/pexels-photo-842554.jpg";
      document.getElementById("myImage").src = img;
    }
  </script>
</body>
</html>
```

### Penjelasan

* Dokumen HTML di atas berisi elemen `<img>` dengan id = "myImage"
* Kita menggunakan DOM HTML untuk mendapatkan elemen dengan id = "myImage"
* JavaScript mengubah atribut `src` elemen itu dari [gambar programmer wanita](https://static.cdn-cdpl.com/340x195/programmer_wanita.jpg) menjadi  [gambar programmer pria](https://static.cdn-cdpl.com/340x195/pexels-photo-842554.jpg). 



