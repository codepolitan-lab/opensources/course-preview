## Javascript Event

Interaksi JavaScript dengan HTML ditangani melalui event. Sebuah event dapat dipicu oleh browser, dapat juga dipicu oleh *user*. Misalnya seperti, saat halaman dimuat, itu disebut sebuah event. Saat *user* mengeklik tombol, klik itu juga merupakan event. 

Untuk menambahkan event Javascript pada HTML, tambahkan kode Javascript pada elemen HTML.

```
<element-HTML jenis-event="Kode JavaScript">
```

Salah satu event Javascript adalah `onClick()`. Ini adalah jenis event yang paling sering digunakan oleh *user*. Event `onClick()` yaitu ketika *user* melakukan klik pada sebuah elemen HTML.

```html runner-html
<!DOCTYPE html>
<html>
<body>

  <button onclick="onClickEvent()">Klik disini</button>

  <p id="demo"></p> 

  <script>
    function onClickEvent() {
      document.getElementById("demo").innerHTML = "Hello World";
    }
  </script>

</body>
</html> 
```

Dimana elemen `<button>` sebagai elemen HTML, `onClick()` sebagai jenis event dan untuk kode Javascript dipanggil melalui function dengan menggunakan atribut event, karena seringkali kode Javascript berbaris panjang.

Berikut adalah list beberapa event pada HTML:

* `onmouseover`, saat pengguna mengarahkan pointer pada sebuah elemen HTML.
* `onmouseout`, saat pengguna memindahkan pointer dari sebuah elemen HTML (kebalikan onmouseover).
* `onkeydown`, saat pengguna menekan sebuah tombol di keyboard.
* `onchange`, saat seuah elemen HTML telah berubah.
* `onload`, saat browser selesai memuat halaman web.

