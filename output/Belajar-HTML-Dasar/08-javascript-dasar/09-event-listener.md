## Event Listener

Event listener adalah suatu event yang diikatkan pada suatu elemen HTML. Untuk mengikatkan suatu event pada elemen HTML menggunakan metode addEventListener(). Contohnya:

```html runner-html
<!DOCTYPE html>
<html>
<body>
  <p>Contoh ini menggunakan metode addEventListener() 
      untuk melampirkan event klik pada tombol.</p>
    
  <button id="myBtn">Try it</button>
  <p id="demo"></p>
    
  <script>
    // daftarkan event click pada button
    document.getElementById("myBtn").addEventListener("click", displayDate);
     
    // deklarasi fungsi yang akan dipanggil
    // oleh event click button
    function displayDate() {
      document.getElementById("demo").innerHTML = Date();
    }
  </script>
</body>
</html>
```

Dalam contoh di atas, tombol dengan id "myBtn" disematkan *event handler* *click* yang akan menjalankan fungsi `displayDate()` jika terpicu.

Metode ini tidak akan menindih event handler yang sudah ada pada elemen. Dengan demikian kita dapat mengikatkan banyak event handler pada sebuah elemen HTML. Bahkan kita dapat mengikat lebih dari satu event handler yang sama ke sebuah elemen HTML. 

Obyek yang dapat diikatkan dengan metode ini tidak hanya obyek elemen HTML saja, misalnya obyek window. Contoh berikut akan menampilkan angka random jika jendela browser berubah ukurannya:

```html
<!DOCTYPE html>
<html>
<body>
  <p>Contoh ini menggunakan metode addEventListener() 
      pada objek window.Cobalah untuk mengubah ukuran 
      window peramban ini untuk memicu pengendali event "resize".</p>
  <p id="demo"></p>
  <script>
    window.addEventListener("resize", function(){
      document.getElementById("demo").innerHTML = Math.random();
    });
  </script>
</body>
</html>
```

