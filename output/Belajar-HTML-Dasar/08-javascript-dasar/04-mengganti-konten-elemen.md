## Mengganti Konten Elemen

HTML DOM memperbolehkan JavaScript mengakses maupun merubah konten elemen. Tidak hanya mengganti/mengubah konten, tetapi juga mengganti/mengubah struktur dan style sebuah dokumen HTML. 

Cara termudah untuk mengganti/mengubah konten elemen HTML adalah dengan menggunakan `innerHTML` properti. Untuk mengganti/mengubah konten elemen HTML, gunakan syntax berikut:

```javascript
document.getElementById(id).innerHTML ="HTML baru"
```

Dimana `getElementById` akan mencari elemen HTML yang memiliki atribut `id` yang diinputkan di dalam tanda kurung dan `innerHTML` menginputkan hasil program kedalam tag ‘*container’* . Yang dimaksud tag *container* adalah tag penampung untuk hasil program JavaScript. Tag *container* bisa berupa tag HTML apapun, seperti tag paragraf `<p>` atau tag `<div>`. Berikut adalah contoh mengubah isi elemen `<p>`:

```html runner-html
<html>
<body>
  <p id="p1">Halo, coders!</p>
  <script>
    document.getElementById("p1").innerHTML = "Semangat belajar!";
  </script>
</body>
</html>
```



### Penjelasan

* Dokumen HTML di atas berisi elemen `<p>` dengan id = "p1"
* Kita menggunakan DOM HTML untuk mendapatkan elemen dengan id = "p1"
* Awalnya paragraf tersebut berisi konten "Halo, coders!". Ketika halaman dimuat dan javascript dijalankan, maka kode tersebut mengubah konten (diwakili oleh property `innerHTML`) dari elemen paragraf tersebut menjadi "Semangat belajar!"



