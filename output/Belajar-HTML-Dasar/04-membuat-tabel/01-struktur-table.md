## Struktur Tabel

Tabel adalah salah satu objek yang umum digunakan dalam sebuah dokumen, termasuk HTML. Tidak seperti elemen HTML lain yang sebelumnya sudah kita pelajari, elemen table dibangun menggunakan setidaknya 3 buah, yaitu `<table>`, `<tr>` dan `<td>`. Struktur penulisan tabel HTML seperti berikut:

```html runner-html
<table border="1">
	<tr>
       	<td>Toni</td>
		<td>Bandung</td>
	</tr>
	<tr>
   		<td>Kresna</td>
   		<td>Jakarta</td>
	</tr>
</table>
```

Tabel HTML dibuat dengan menggunakan tag `<table>`. Pada contoh di atas kita menggunakan atribut `border="1"` agar tabel memiliki garis. Jikalau tidak membutuhkan border, maka kita dapat menggunakan `border = "0"` atau dengan tidak menuliskan atribut tersebut. 

Tag `<tr>` digunakan untuk membuat baris tabel.  Sedangkan tag `<td>` digunakan untuk membuat kolom sel di dalam baris. Konten disimpan di dalam tag `<td>` ini.

Perhatikan bahwa struktur penulisannya mesti tepat seperti contoh di atas, yakni tag `<tr>` harus berada di dalam tag `<table>`. Tidak boleh ada elemen lain yang menjadi anak dari tag `<table>` selain `<tr>`.

Perhatikan pula bahwa tag `<td>` harus disimpan di dalam tag `<tr>`. TIdak boleh membuat tag `<td>` diluar elemen `<tr>`. Dan di dalam elemen `<tr>` tidak boleh ada elemen lain selain `<td>`.

Bila Kamu mau menampilkan konten atau gambar, atau elemen lainnya di dalam table, Kamu harus menyimpannya di dalam elemen `<td>`.