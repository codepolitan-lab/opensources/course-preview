## Header Tabel

Seringkali dalam membuat tabel, kita perlu menampilkan header/judul untuk setiap kolom pada tabel. Header/judul tabel akan tampil berbeda dengan baris tabel di bawahnya. Untuk keperluan ini, HTML menyediakan tag `<th>` (singkatan dari **table head**) untuk memuat konten judul kolom. Penggunaan tag `<th>` menggantikan tag `<td>`.

```html runner-html
<table border="1">
	<tr>
		<th>Nama</th>
		<th>Kota</th>
		<th>Gol. Darah</th>
	</tr>
	<tr>
		<td>Toni</td>
		<td>Bandung</td>
		<td>B</td>
	</tr>
	<tr>
		<td>Kresna</td>
		<td>Jakarta</td>
		<td>AB</td>
	</tr>
	<tr>
		<td>Singgih</td>
		<td>Cianjur</td>
		<td>A</td>
	</tr>
</table>
```

Dalam penggunaan tag `<th>` seperti contoh kode di atas. Tag `<th>` secara default akan ditampilkan berbeda, yakni dicetak tebal, tidak seperti kolom lainnya.