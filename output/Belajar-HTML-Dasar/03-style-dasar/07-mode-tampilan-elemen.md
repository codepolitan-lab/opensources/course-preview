## Mode Tampilan Elemen

### Block dan Inline

Pada bagian terdahulu tentang elemen *inline* dan *block*, kita sudah mengetahui bahwa ada elemen-elemen yang ditampilkan dalam satu baris bersama elemen lain (*inline*), dan ada juga elemen yang ditampilkan dalam baris tersendiri (*block*). Adakalanya kita perlu mengubah mode tampilan bawaan dari suatu elemen. Untuk itu kita gunakan property `display`.

Misalnya elemen `<img>` memiliki mode *inline*, maka bila kita menampilkan elemen gambar beriringan dengan teks, gambar tersebut akan ditampilkan satu baris dengan teks. Untuk membuat elemen `<img/>` agar ditampilkan dalam baris tersendiri, maka kita perbaharui mode tampilannya menjadi block menggunakan property `display:block;`.

```html
<img src="img/kucing-kecil.jpg" />
<img src="img/kucing-besar.jpg" />
```

Pada contoh di atas, bila kita tidak menambahkan style `display:block` pada elemen `<img>`, maka kedua gambar akan ditampilkan dalam satu baris.

![](../images/03-07-kitties.png) 

Untuk menampilkan kedua gambar tersebut menumpuk ke bawah, kita dapat ubah property `display`nya menjadi block. 

```html
<style>
    img {display:block;}
</style>

<img src="img/kucing-kecil.jpg" />
<img src="img/kucing-besar.jpg" />
```

![](../images/03-07-kitties-stacked.png) 

### Menyembunyikan Elemen

Selain `inline` dan `block`, ada value lain untuk property `display`, salahsatunya adalah `none`. Nilai `none` dapat kita gunakan untuk menyembunyikan elemen. Misalnya, untuk menyembunyikan pesan error yang hanya ditampilkan bila muncul kesalahan pada aplikasi, atau menyembunyikan konten dari tab yang tidak dipilih.

```html runner-html
<div class="warning" style="color:red; display:none">
    Terjadi kesalahan pada saat menyimpan data.
</div>

<p>
    Elemen di atas tidak akan ditampilkan pada layar browser,
    kecuali bila kita mengubah nilai property display menjadi block.
</p>
```

