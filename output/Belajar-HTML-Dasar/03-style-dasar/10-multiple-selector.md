## Multiple Selector

Kita dapat membuat style untuk setiap elemen, namun bisa jadi tidak efisien bila kita harus menuliskan kode yang sama berulang. Contohnya jika kita mempunyai dua elemen yang berbeda ( `<h2>` dan `<h3>`), namun kita menginginkan style yang sama untuk kedua elemen tersebut:

```css
h2 {color:red;}
h3 {color:red;}
```

kode di atas dapat ditulis lebih sederhana menjadi:

```css
h2, h3 {color:red;}
```

Kita dapat menggunakan tanda koma ( , ) sebagai pemisah antara elemen yang ingin dipilih. Tidak ada batasan seberapa banyak selector yang digunakan dalam satu deklarasi. Penggunaan banyak selector ini dapat digunakan untuk semua jenis selector.

```html runner-html
<style>
h1, p { color:orange; }
#highlight { background:yellow; }
</style>

<h1>Ini Judul</h1>
<p>Ini paragraf.</p>
<p id="highlight">Ini paragraf dengan latar warna kuning.</p>
<p>Ini paragraf lain berteks biasa.</p>
<p>Semua elemen paragraf dan heading berwarna teks oranye.</p>
```

