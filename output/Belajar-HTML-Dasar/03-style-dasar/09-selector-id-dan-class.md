## Selector id dan class

Selain selector berupa nama tag elemen, terdapat juga selector yang merujuk pada nilai atribut `id` dan `class` pada elemen. Fungsinya adalah untuk memilih elemen yang lebih spesifik dibanding bila kita memberi style dengan nama elemennya.

### ID Selector

Selector `id`  digunakan untuk menyeleksi elemen berdasarkan `id` tertentu. Dalam penggunaannya, selector `id` diawali dengan tanda pagar (`#`). 

```html
<style>
p { font-size:14px; }
#highlight { background:yellow; }
</style>

<p>Ini paragraf.</p>
<p id="highlight">Ini paragraf dengan latar warna kuning.</p>
<p>Ini paragraf lain berteks biasa.</p>
```

Pada contoh di atas, kita punya elemen `<p>` kedua yang memiliki atribut `id`. Nilai atribut `id`nya adalah `"highlight"`. Nama id inilah yang akan kita gunakan sebagai selector, yang pada contoh di atas ditulis menjadi `#highlight`.

Di dalam sebuah dokumen tidak boleh terdapat lebih dari satu elemen dengan nilai `id` yang sama. Artinya, nilai dari sebuah atribut `id` hanya boleh dimiliki oleh satu elemen saja. Pada contoh di atas, hanya satu elemen `p` saja yang boleh memiliki atribut `id="highlight"`.

Nama `id` hanya boleh mengandung huruf dan angka. Selain itu penamaan nilai `id` juga tidak boleh diawali oleh angka seperti misalnya `id="1tebal"`.  Tetapi Kamu boleh menggunakan angka pada huruf kedua dan seterusnya dari nama id, misalnya `id="tebal1"` atau `id="tebal1-teks"`.

## Class Selector

`class` selector digunakan untuk menyeleksi elemen berdasarkan nama class atau nilai dari attribute class. Cara penulisan selector untuk class adalah dengan menuliskan nama class diawali dengan tanda titik (`.`)

```html
<style>
p { font-size:14px; }
p.tebal { font-weight:bold; }
</style>

<p>Ini paragraf.</p>
<p class="tebal">Ini paragraf berteks tebal.</p>
<p class="tebal">Ini paragraf lain berteks biasa.</p>
```

Pada contoh kode di atas kita punya beberapa elemen `<p>` yang memiliki atribut class dengan nilai `tebal`. Nama class inilah yang akan kita gunakan sebagai selector, yang pada contoh di atas ditulis menjadi `.tebal`.

Berbeda dengan selector `id`, selector `class` dapat digunakan pada lebih dari satu elemen. Keuntungan menggunakan selector `class` ini adalah kita dapat mengenakan style pada lebih dari satu elemen tertentu yang perlu ditampilkan sama.