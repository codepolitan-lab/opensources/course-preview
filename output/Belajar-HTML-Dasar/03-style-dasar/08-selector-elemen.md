## Selector Elemen

Pada bagian sebelumnya kita telah melihat pendeklarasian style di dalam tag `<style>`.

``` css
selector { property: value }
```

Deklarasi style yang disimpan di luar tag elemennya disebut dengan CSS atau *Cascading Style Sheet*. Karena style disimpan di luar elemen, maka harus ada bagian yang berfungsi untuk memberi tahu elemen mana yang akan dikenai oleh style. Bagian tersebut disebut dengan selector.

Selector adalah pola yang dikenali oleh browser untuk memilih elemen HTML yang ingin dikenai style. selector ditulis di bagian awal deklarasi style, dan deklarasi style ditulis di dalam kurung kurawal. Selector paling sederhana adalah dengan menggunakan nama tag dari elemen yang dipilih.

```html runner-html
<style>
p { 
    font-size:20px; 
    color:blue; 
}
strong {
	background:yellow;  
}
</style>

<p>Ini paragraf <strong>pertama</strong>.</p>
<p>Ini paragraf <strong>kedua</strong>.</p>
```

Pada kode di atas terdapat CSS dengan selector elemen menunjuk ke elemen `<p>`. Seluruh elemen `<p>` pada dokumen akan dikenai style ukuran font 20px dan teks berwarna biru. Selain itu juga terdapat CSS untuk elemen `<strong>` sehingga semua elemen tersebut akan dikenai style khusus yakni background berwarna kuning.

Perhatikan bahwa elemen `<strong>` berada di dalam elemen `<p>` sehingga elemen `<strong>` pun dikenai style yang sama seperti yang dikenakan pada induk elemennya (ukuran font 18px dan warna teks biru).
