# CodePolitan Course Preview

- Clone aplikasi `git clone --depth=1 git@gitlab.com:codepolitan-lab/opensources/course-preview.git`
- simpan folder project berdampingan dengan folder course, misalnya:
  ```
  - belajar-php-dasar
  - belajar-html-dasar
  - course-preview  <-- ini folder aplikasinya
  ```
- Pastikan di dalam folder course terdapat file `course.yml` agar dapat terbaca oleh aplikasi
- Buka terminal pada folder `course-preview`
- Jalankan perintah `php -S localhost:8000`
- Buka browser dan ketikkan url `http://localhost:8000` untuk menampilkan preview course
- Kamu juga dapat menggunakan XAMPP dan semisalnya untuk menjalankan aplikasi ini